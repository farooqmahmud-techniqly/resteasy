﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Techniqly.RestEasy.Lib.Credentials;
using Techniqly.RestEasy.Lib.Decorators;
using Techniqly.RestEasy.Lib.Factories;
using Techniqly.RestEasy.Lib.Infrastructure;

namespace Techniqly.RestEasy.Lib
{
    public abstract class Request : IRequest, IDisposable
    {
        private readonly List<IRequestDecorator> _decorators;
        private readonly DelegatingHandler _httpHandler;
        private readonly HttpMethod _httpMethod;
        private bool _disposed;
        private readonly string _uri;
        private readonly IResponseFactory _responseFactory;
        private readonly IHttpRequestMessageFactory _requestMessageFactory;

        protected Request(string uri, HttpMethod httpMethod)
            :this(uri, httpMethod, new DecoratorFactory(), new HttpRequestMessageFactory(), new ResponseFactory(), new RequestHandler())
        {
            
        }
        protected Request(string uri, HttpMethod httpMethod, DelegatingHandler httpHandler)
            : this(uri, httpMethod, new DecoratorFactory(), new HttpRequestMessageFactory(), new ResponseFactory(), httpHandler)
        {
        }

        protected Request(string uri, HttpMethod httpMethod, IDecoratorFactory decoratorFactory, IHttpRequestMessageFactory requestMessageFactory, IResponseFactory responseFactory, DelegatingHandler httpHandler)
        {
            _uri = uri;
            _httpMethod = httpMethod;
            _decorators = decoratorFactory.GetRequestDecorators().ToList();
            _httpHandler = httpHandler;
            _responseFactory = responseFactory;
            _requestMessageFactory = requestMessageFactory;

            QueryParameters = new NameValueCollection();
            CustomHeaders = new NameValueCollection();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        
        public NameValueCollection QueryParameters { get; set; }
        public string Content { get; set; }
        public Credential Credential { get; set; }
        public NameValueCollection CustomHeaders { get; set; }

        public async Task<HttpResponse> ExecuteAsync()
        {
            using (var httpClient = new HttpClient(_httpHandler))
            {
                var httpRequest = _requestMessageFactory.CreateHttpRequestMessage(_httpMethod, _uri);
                ExecuteRequestDecorators(httpRequest);

                var httpResponse = await ExecuteRequestAsync(httpClient, httpRequest);

                return await _responseFactory.CreateResponseAsync(this, httpResponse);
            }
        }

        private static async Task<HttpResponseMessage> ExecuteRequestAsync(HttpClient httpClient, HttpRequestMessage httpRequest)
        {
            var httpResponse = await httpClient.SendAsync(httpRequest);
            return httpResponse;
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
                _httpHandler.Dispose();
            }

            _disposed = true;
        }

        private void ExecuteRequestDecorators(HttpRequestMessage httpRequest)
        {
            foreach (var requestDecorator in _decorators)
            {
                requestDecorator.Execute(this, httpRequest);
            }
        }
    }
}