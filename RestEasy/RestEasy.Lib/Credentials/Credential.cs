﻿using System;
using System.Text;

namespace Techniqly.RestEasy.Lib.Credentials
{
    public sealed class Credential
    {
        public Credential(string username, string password)
        {
            if (string.IsNullOrWhiteSpace(username))
            {
                throw new ArgumentNullException(nameof(username));
            }

            if (string.IsNullOrWhiteSpace(password))
            {
                throw new ArgumentNullException(nameof(password));
            }

            Username = username;
            Password = password;
        }

        public string Username { get; }
        public string Password { get; }

        public override string ToString()
        {
            return Base64Encode($"{Username}:{Password}");
        }

        private static string Base64Encode(string plaintext)
        {
            var bytes = Encoding.UTF8.GetBytes(plaintext);
            return Convert.ToBase64String(bytes);
        }
    }
}