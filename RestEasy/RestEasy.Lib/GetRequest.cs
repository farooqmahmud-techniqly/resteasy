﻿using System.Net.Http;
using Techniqly.RestEasy.Lib.Infrastructure;

namespace Techniqly.RestEasy.Lib
{
    public sealed class GetRequest : Request
    {
        public GetRequest(string uri) :this(uri, new RequestHandler())
        {
            
        }

        public GetRequest(string uri, DelegatingHandler httpHandler) : base(uri, HttpMethod.Get, httpHandler)
        {
        }
    }
}