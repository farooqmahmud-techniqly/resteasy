﻿using System.Net.Http;

namespace Techniqly.RestEasy.Lib
{
    public sealed class DeleteRequest : Request
    {
        public DeleteRequest(string uri, DelegatingHandler httpHandler) : base(uri, HttpMethod.Delete, httpHandler)
        {
        }
    }
}