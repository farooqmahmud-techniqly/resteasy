﻿using System.Collections.Specialized;
using System.Threading.Tasks;
using Techniqly.RestEasy.Lib.Credentials;

namespace Techniqly.RestEasy.Lib
{
    public interface IRequest
    {
        NameValueCollection QueryParameters { get; set; }
        Credential Credential { get; set; }
        Task<HttpResponse> ExecuteAsync();
    }
}