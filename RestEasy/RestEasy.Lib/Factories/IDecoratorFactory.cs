﻿using System.Collections.Generic;
using Techniqly.RestEasy.Lib.Decorators;

namespace Techniqly.RestEasy.Lib.Factories
{
    public interface IDecoratorFactory
    {
        IEnumerable<IRequestDecorator> GetRequestDecorators();
    }
}