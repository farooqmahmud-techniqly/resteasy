﻿using System.Collections.Generic;
using Techniqly.RestEasy.Lib.Decorators;

namespace Techniqly.RestEasy.Lib.Factories
{
    internal sealed class DecoratorFactory : IDecoratorFactory
    {
        public IEnumerable<IRequestDecorator> GetRequestDecorators()
        {
            yield return new QueryStringDecorator();
            yield return new AcceptHeaderDecorator();
            yield return new ContentDecorator();
            yield return new ContentTypeDecorator();
            yield return new BasicAuthenticationDecorator();
            yield return new CustomHeaderDecorator();
        }
    }
}