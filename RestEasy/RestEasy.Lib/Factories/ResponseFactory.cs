﻿using System.Net.Http;
using System.Threading.Tasks;

namespace Techniqly.RestEasy.Lib.Factories
{
    internal sealed class ResponseFactory : IResponseFactory
    {
        public async Task<HttpResponse> CreateResponseAsync(Request request, HttpResponseMessage responseMessage)
        {
            var response = new HttpResponse
            {
                StatusCode = (int) responseMessage.StatusCode,
                RawResponse = responseMessage
            };

            if (responseMessage.Content == null)
            {
                return response;
            }

            var content = await responseMessage.Content.ReadAsStringAsync();
            response.Content = content;

            return response;
        }
    }
}