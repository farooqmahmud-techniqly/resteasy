﻿using System.Net.Http;
using System.Threading.Tasks;

namespace Techniqly.RestEasy.Lib.Factories
{
    public interface IResponseFactory
    {
        Task<HttpResponse> CreateResponseAsync(Request request, HttpResponseMessage responseMessage);
    }
}