﻿using System.Net.Http;

namespace Techniqly.RestEasy.Lib.Factories
{
    public interface IHttpRequestMessageFactory
    {
        HttpRequestMessage CreateHttpRequestMessage(HttpMethod httpMethod, string uri);
    }
}