﻿using System;
using System.Net.Http;

namespace Techniqly.RestEasy.Lib.Factories
{
    internal sealed class HttpRequestMessageFactory : IHttpRequestMessageFactory
    {
        public HttpRequestMessage CreateHttpRequestMessage(HttpMethod httpMethod, string uri)
        {
            return new HttpRequestMessage(httpMethod, new Uri(uri));
        }
    }
}