﻿using System.Net.Http;
using Techniqly.RestEasy.Lib.Infrastructure;

namespace Techniqly.RestEasy.Lib
{
    public sealed class PostRequest : Request
    {
        public PostRequest(string uri) :this(uri, new RequestHandler())
        {

        }

        public PostRequest(string uri, DelegatingHandler httpHandler) : base(uri, HttpMethod.Post, httpHandler)
        {
        }
    }
}