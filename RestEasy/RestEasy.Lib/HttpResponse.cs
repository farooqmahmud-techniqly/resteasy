﻿
using System.Net.Http;

namespace Techniqly.RestEasy.Lib
{
    public sealed class HttpResponse
    {
        public string Content { get; internal set; }
        public int StatusCode { get; set; }
        public HttpResponseMessage RawResponse { get; set; }
    }
}