﻿using System.Net.Http;
using Techniqly.RestEasy.Lib.Infrastructure;

namespace Techniqly.RestEasy.Lib
{
    public sealed class PutRequest : Request
    {
        public PutRequest(string uri) :this(uri, new RequestHandler())
        {

        }

        public PutRequest(string uri, DelegatingHandler httpHandler) : base(uri, HttpMethod.Put, httpHandler)
        {
        }
    }
}