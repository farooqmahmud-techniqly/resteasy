﻿using System;
using System.Net.Http;
using Techniqly.RestEasy.Lib.Infrastructure;

namespace Techniqly.RestEasy.Lib.Decorators
{
    internal sealed class QueryStringDecorator : RequestDecorator
    {
        protected override bool DiscardRequest(Request request, HttpRequestMessage httpRequest)
        {
            return !request.QueryParameters.HasKeys();
        }

        protected override void DoExecute(Request request, HttpRequestMessage httpRequest)
        {
            AddQueryParameters(request, httpRequest);
        }

        private static void AddQueryParameters(Request request, HttpRequestMessage requestMessage)
        {
            var uri = requestMessage.RequestUri.ToString();

            if (uri[uri.Length - 1] == '/')
            {
                uri = uri.Remove(uri.Length - 1);
            }

            var qb = new QueryParameterBuilder(request.QueryParameters);
            qb.Build();

            uri = uri + qb;
            requestMessage.RequestUri = new Uri(uri);
        }
    }
}