﻿using System.Net.Http;
using System.Net.Http.Headers;

namespace Techniqly.RestEasy.Lib.Decorators
{
    internal sealed class ContentTypeDecorator : RequestDecorator
    {
        protected override bool DiscardRequest(Request request, HttpRequestMessage httpRequest)
        {
            if (httpRequest.Method != HttpMethod.Post && httpRequest.Method != HttpMethod.Put)
            {
                return true;
            }

            return request.Content == null;
        }

        protected override void DoExecute(Request request, HttpRequestMessage httpRequest)
        {
            httpRequest.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
        }
    }
}