﻿using System.Net.Http;
using System.Net.Http.Headers;
using Techniqly.RestEasy.Lib.Infrastructure;

namespace Techniqly.RestEasy.Lib.Decorators
{
    internal sealed class AcceptHeaderDecorator : RequestDecorator
    {
        protected override void DoExecute(Request request, HttpRequestMessage httpRequest)
        {
            httpRequest.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue(Strings.JsonContentType));
        }
    }
}