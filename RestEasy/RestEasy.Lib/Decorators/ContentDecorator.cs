﻿using System.Net.Http;

namespace Techniqly.RestEasy.Lib.Decorators
{
    internal  sealed class ContentDecorator : RequestDecorator
    {
        protected override bool DiscardRequest(Request request, HttpRequestMessage httpRequest)
        {
            if (httpRequest.Method != HttpMethod.Post && httpRequest.Method != HttpMethod.Put)
            {
                return true;
            }

            return request.Content == null;
        }

        protected override void DoExecute(Request request, HttpRequestMessage httpRequest)
        {
            httpRequest.Content = new StringContent(request.Content);
        }
    }
}