﻿using System.Net.Http;

namespace Techniqly.RestEasy.Lib.Decorators
{
    internal sealed class CustomHeaderDecorator : RequestDecorator
    {
        protected override bool DiscardRequest(Request request, HttpRequestMessage httpRequest)
        {
            return !request.CustomHeaders.HasKeys();
        }

        protected override void DoExecute(Request request, HttpRequestMessage httpRequest)
        {
            foreach (var key in request.CustomHeaders.AllKeys)
            {
                httpRequest.Headers.Add(key, request.CustomHeaders[key]);
            }
        }
    }
}