﻿using System.Net.Http;

namespace Techniqly.RestEasy.Lib.Decorators
{
    internal abstract class RequestDecorator : IRequestDecorator
    {
        public void Execute(Request request, HttpRequestMessage httpRequest)
        {
            if (request == null || httpRequest == null)
            {
                return;
            }

            if (DiscardRequest(request, httpRequest))
            {
                return;
            }

            DoExecute(request, httpRequest);
        }

        protected abstract void DoExecute(Request request, HttpRequestMessage httpRequest);

        protected virtual bool DiscardRequest(Request request, HttpRequestMessage httpRequest)
        {
            return false;
        }
    }
}