﻿using System.Net.Http;

namespace Techniqly.RestEasy.Lib.Decorators
{
    public interface IRequestDecorator
    {
        void Execute(Request request, HttpRequestMessage httpRequest);
    }
}