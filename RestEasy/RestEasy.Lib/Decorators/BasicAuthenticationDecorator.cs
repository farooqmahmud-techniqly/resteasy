﻿using System.Net.Http;
using System.Net.Http.Headers;
using Techniqly.RestEasy.Lib.Infrastructure;

namespace Techniqly.RestEasy.Lib.Decorators
{
    internal sealed class BasicAuthenticationDecorator : RequestDecorator
    {
        protected override bool DiscardRequest(Request request, HttpRequestMessage httpRequest)
        {
            return request.Credential == null;
        }

        protected override void DoExecute(Request request, HttpRequestMessage httpRequest)
        {
            httpRequest.Headers.Authorization = new AuthenticationHeaderValue(Strings.BasicAuthentication, request.Credential.ToString());
        }
    }
}