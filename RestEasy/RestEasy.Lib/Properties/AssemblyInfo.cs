﻿using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Techniqly.RestEasy.Lib")]
[assembly: AssemblyDescription("An easy to use library for working with REST API's.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Techniqly")]
[assembly: AssemblyProduct("Techniqly.RestEasy.Lib")]
[assembly: AssemblyCopyright("Copyright © Techniqly 2016")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]
[assembly: Guid("dfd2ecd1-aa15-4b2e-a83d-b5d3074074f7")]
[assembly: AssemblyVersion("1.1.*")]
[assembly: AssemblyFileVersion("1.1.*")]
[assembly: CLSCompliant(true)]
[assembly:InternalsVisibleTo("Techniqly.RestEasy.Tests")]