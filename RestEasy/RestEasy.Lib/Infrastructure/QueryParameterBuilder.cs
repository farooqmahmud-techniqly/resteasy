﻿using System.Collections.Specialized;
using System.Text;

namespace Techniqly.RestEasy.Lib.Infrastructure
{
    internal sealed class QueryParameterBuilder
    {
        private readonly NameValueCollection _queryParameters;
        private readonly StringBuilder _sb;

        internal QueryParameterBuilder(NameValueCollection queryParameters)
        {
            _queryParameters = queryParameters;
            _sb = new StringBuilder();
        }

        internal void Build()
        {
            BuildQueryString();
            CleanupQueryString();
        }

        private void CleanupQueryString()
        {
            if (_sb[_sb.Length - 1] == '&')
            {
                _sb.Remove(_sb.Length - 1, 1);
            }

            _sb.Insert(0, '?');
        }

        private void BuildQueryString()
        {
            foreach (var key in _queryParameters.AllKeys)
            {
                _sb.Append($"{key}={_queryParameters[key]}&");
            }
        }

        public override string ToString()
        {
            return _sb.ToString();
        }
    }
}