﻿using System.Net.Http;

namespace Techniqly.RestEasy.Lib.Infrastructure
{
    internal sealed class RequestHandler : DelegatingHandler
    {
        public RequestHandler()
        {
            InnerHandler = new HttpClientHandler();
        }
    }
}