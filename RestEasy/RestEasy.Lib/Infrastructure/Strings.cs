﻿namespace Techniqly.RestEasy.Lib.Infrastructure
{
    internal static class Strings
    {
        internal static string JsonContentType = "application/json";
        internal static string BasicAuthentication = "Basic";
    }
}