﻿using System.Net;
using System.Threading.Tasks;
using FluentAssertions;
using NUnit.Framework;
using Techniqly.RestEasy.Lib;
using Techniqly.RestEasy.Tests.UnitTests;

namespace Techniqly.RestEasy.Tests
{
    [TestFixture]
    public class PostRequestUnitTests : RequestUnitTests
    {
        [SetUp]
        public void SetUp()
        {
            TestInitialize();
            Handler = CreateDefaultMockDelegatingHandler(_expectedStatusCode);
            Request = new PostRequest(DefaultUri, Handler) {Content = _content};
        }

        [TearDown]
        public void TearDown()
        {
            TestCleanup();
        }

        private static readonly string _content = "{\"models\":[\"435i\", \"640i\", \"i8\"]}";
        private static readonly HttpStatusCode _expectedStatusCode = HttpStatusCode.Created;

        [Test]
        public async Task PostContent()
        {
            Request.Content = "{\"make\":\"bmw\", \"model\":\"435i\"}";
            var response = await Request.ExecuteAsync();

            response.Content.Should().BeNull();
            response.StatusCode.Should().Be((int) _expectedStatusCode);
        }
    }
}