﻿using System.Net;
using System.Threading.Tasks;
using FluentAssertions;
using NUnit.Framework;
using Techniqly.RestEasy.Lib;

namespace Techniqly.RestEasy.Tests.UnitTests
{
    [TestFixture]
    public class DeleteRequestUnitTests : RequestUnitTests
    {
        [SetUp]
        public void SetUp()
        {
            TestInitialize();
            Handler = CreateDefaultMockDelegatingHandler(_expectedStatusCode);
            Request = new DeleteRequest(DefaultUri, Handler);
            Request.QueryParameters.Add("id", "12345");
        }

        [TearDown]
        public void TearDown()
        {
            TestCleanup();
        }

        private static readonly HttpStatusCode _expectedStatusCode = HttpStatusCode.NoContent;

        [Test]
        public async Task Delete()
        {
            var response = await Request.ExecuteAsync();

            response.Content.Should().BeNull();
            response.StatusCode.Should().Be((int)_expectedStatusCode);
        }
    }
}
