﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;
using Techniqly.RestEasy.Lib;
using Techniqly.RestEasy.Lib.Factories;
using Techniqly.RestEasy.Tests.Mocks;

namespace Techniqly.RestEasy.Tests.UnitTests
{
    [TestFixture]
    public class ResponseFactoryUnitTests
    {
        [Test]
        public async Task CreateResponseAsync_AddsRawResponse()
        {
            var factory = new ResponseFactory();
            var request = new GetRequest("http://www.blah.com", new MockDelegatingHandler(HttpStatusCode.OK));
            var response = await factory.CreateResponseAsync(request, new HttpResponseMessage());
            response.RawResponse.Should().NotBeNull();
        }
    }
}
