﻿using System.Net.Http;
using System.Threading.Tasks;
using NUnit.Framework;
using Techniqly.RestEasy.Lib.Credentials;
using Techniqly.RestEasy.Lib.Infrastructure;
using Techniqly.RestEasy.Tests.Mocks;

namespace Techniqly.RestEasy.Tests.UnitTests.Decorators
{
    [TestFixture]
    public class BasicAuthenticationDecoratorUnitTests : DecoratorUnitTests
    {
        [Test]
        public async Task BasicAuthenticationDecoratorAddsCredentialsToRequest()
        {
            var username = "farooq";
            var password = "1234";
            var expectedParameter = "ZmFyb29xOjEyMzQ=";

            var handler = CreateMockDelegatingHandler(requestMessage => requestMessage.Headers.Authorization.Scheme == Strings.BasicAuthentication &&
                                                                    requestMessage.Headers.Authorization.Parameter == expectedParameter);

            TestRequest = new TestRequest(DefaultUri, HttpMethod.Get, handler)
            {
                Credential = new Credential(username, password)
            };

            await TestRequest.ExecuteAsync();
        }
    }
}