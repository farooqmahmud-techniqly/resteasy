﻿using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using NUnit.Framework;
using Techniqly.RestEasy.Tests.Mocks;

namespace Techniqly.RestEasy.Tests.UnitTests.Decorators
{
    [TestFixture]
    public class CustomHeaderDecoratorUnitTests : DecoratorUnitTests
    {
        [Test]
        public async Task CustomHeaderDecoratorAddsCustomHeaderToRequest()
        {
            var customHeaderName = "my-custom-header";
            var customHeaderValue = "value";

            var handler = CreateMockDelegatingHandler(requestMessage =>
            {
                var customHeaderValues = requestMessage.Headers.First(kvp => kvp.Key == customHeaderName);
                return customHeaderValues.Value.Contains(customHeaderValue);
            });

            TestRequest = new TestRequest(DefaultUri, HttpMethod.Get, handler);
            TestRequest.CustomHeaders.Add(customHeaderName, customHeaderValue);

            await TestRequest.ExecuteAsync();
        }
    }
}