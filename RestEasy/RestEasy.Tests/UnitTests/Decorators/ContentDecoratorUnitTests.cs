﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using NUnit.Framework;
using Techniqly.RestEasy.Tests.Mocks;

namespace Techniqly.RestEasy.Tests.UnitTests.Decorators
{
    [TestFixture]
    public class ContentDecoratorUnitTests : DecoratorUnitTests
    {
        private readonly Predicate<HttpRequestMessage> _contentPresentAssertion =
            requestMessage => requestMessage.Content != null;

        private readonly Predicate<HttpRequestMessage> _contentNotPresentAssertion =
            requestMessage => requestMessage.Content == null;

        private static readonly string DefaultContent = "{\"make\":\"BMW\"}";


        private MockDelegatingHandler CreateMockDelegatingHandler(bool expectContentTypePresent)
        {
            return
                CreateMockDelegatingHandler(expectContentTypePresent
                    ? _contentPresentAssertion
                    : _contentNotPresentAssertion);
        }

        private void InitializeTestRequest(HttpMethod httpMethod, bool expectContentTypePresent, string content = null)
        {
            var handler = CreateMockDelegatingHandler(expectContentTypePresent);
            TestRequest = new TestRequest(DefaultUri, httpMethod, handler);

            if (content != null)
            {
                TestRequest.Content = content;
            }
        }

        [Test]
        public async Task ContentDecoratorAddsContentTypeForPost()
        {
            InitializeTestRequest(HttpMethod.Post, true, DefaultContent);
            await TestRequest.ExecuteAsync();
        }

        [Test]
        public async Task ContentDecoratorAddsContentTypeForPut()
        {
            InitializeTestRequest(HttpMethod.Put, true, DefaultContent);
            await TestRequest.ExecuteAsync();
        }

        [Test]
        public async Task ContentDecoratorDoesNotAddContentTypeForDelete()
        {
            InitializeTestRequest(HttpMethod.Delete, false);
            await TestRequest.ExecuteAsync();
        }

        [Test]
        public async Task ContentDecoratorDoesNotAddContentTypeForGet()
        {
            InitializeTestRequest(HttpMethod.Get, false);
            await TestRequest.ExecuteAsync();
        }
    }
}