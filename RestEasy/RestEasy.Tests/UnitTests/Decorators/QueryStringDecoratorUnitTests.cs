﻿using System.Net.Http;
using System.Threading.Tasks;
using NUnit.Framework;
using Techniqly.RestEasy.Tests.Mocks;

namespace Techniqly.RestEasy.Tests.UnitTests.Decorators
{
    [TestFixture]
    public class QueryStringDecoratorUnitTests : DecoratorUnitTests
    {
        [Test]
        public async Task QueryStringAddedToRequestUri()
        {
            var expectedUri = DefaultUri + "?model=435i&year=2016";
            var handler = CreateMockDelegatingHandler(requestMessage => requestMessage.RequestUri.ToString() == expectedUri);

            TestRequest = new TestRequest(DefaultUri, HttpMethod.Get, handler);
            TestRequest.QueryParameters.Add("model", "435i");
            TestRequest.QueryParameters.Add("year", "2016");

            await TestRequest.ExecuteAsync();
        }

        [Test]
        public async Task QueryStringAddedToRequestUri_UriHasTrailingSlash()
        {
            var expectedUri = DefaultUri + "?model=435i&year=2016";
            var handler = CreateMockDelegatingHandler(requestMessage => requestMessage.RequestUri.ToString() == expectedUri);

            TestRequest = new TestRequest(DefaultUri + "/", HttpMethod.Get, handler);
            TestRequest.QueryParameters.Add("model", "435i");
            TestRequest.QueryParameters.Add("year", "2016");

            await TestRequest.ExecuteAsync();
        }
    }
}