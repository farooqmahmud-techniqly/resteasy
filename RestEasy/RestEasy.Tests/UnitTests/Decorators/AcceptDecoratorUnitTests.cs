﻿using System.Net.Http;
using System.Threading.Tasks;
using NUnit.Framework;
using Techniqly.RestEasy.Lib.Infrastructure;
using Techniqly.RestEasy.Tests.Mocks;

namespace Techniqly.RestEasy.Tests.UnitTests.Decorators
{
    [TestFixture]
    public class AcceptDecoratorUnitTests : DecoratorUnitTests
    {
        
        [Test]
        public async Task JsonContentTypeAddedToRequest()
        {
            var handler = CreateMockDelegatingHandler(requestMessage => requestMessage.Headers.Accept.ToString() == Strings.JsonContentType);

            TestRequest = new TestRequest(DefaultUri, HttpMethod.Get, handler);
            await TestRequest.ExecuteAsync();
        }
    }
}