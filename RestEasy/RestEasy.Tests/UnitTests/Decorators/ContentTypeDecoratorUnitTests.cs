﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using NUnit.Framework;
using Techniqly.RestEasy.Tests.Mocks;

namespace Techniqly.RestEasy.Tests.UnitTests.Decorators
{
    [TestFixture]
    public class ContentTypeDecoratorUnitTests : DecoratorUnitTests
    {
        private readonly Predicate<HttpRequestMessage> _jsonContentTypePresentAssertion = requestMessage => requestMessage.Content.Headers.ContentType.MediaType == "application/json";
        private readonly Predicate<HttpRequestMessage> _jsonContentTypeNotPresentAssertion = (requestMessage) => requestMessage.Content == null;
        private static readonly string DefaultContent = "{\"make\":\"BMW\"}";

        [Test]
        public async Task ContentTypeDecoratorAddsContentTypeForPost()
        {
            InitializeTestRequest(HttpMethod.Post, true, DefaultContent);
            await TestRequest.ExecuteAsync();
        }

        [Test]
        public async Task ContentTypeDecoratorAddsContentTypeForPut()
        {
            InitializeTestRequest(HttpMethod.Put, true, DefaultContent);
            await TestRequest.ExecuteAsync();
        }

        [Test]
        public async Task ContentTypeDecoratorDoesNotAddContentTypeForGet()
        {
            InitializeTestRequest(HttpMethod.Get, false);
            await TestRequest.ExecuteAsync();
        }

        [Test]
        public async Task ContentTypeDecoratorDoesNotAddContentTypeForDelete()
        {
            InitializeTestRequest(HttpMethod.Delete, false);
            await TestRequest.ExecuteAsync();
        }


        private MockDelegatingHandler CreateMockDelegatingHandler(bool expectContentTypePresent)
        {
            return CreateMockDelegatingHandler(expectContentTypePresent ? _jsonContentTypePresentAssertion : _jsonContentTypeNotPresentAssertion);
        }

        private void InitializeTestRequest(HttpMethod httpMethod, bool expectContentTypePresent, string content = null)
        {
            var handler = CreateMockDelegatingHandler(expectContentTypePresent);
            TestRequest = new TestRequest(DefaultUri, httpMethod, handler);

            if (content != null)
            {
                TestRequest.Content = content;
            }   
        }
    }
}