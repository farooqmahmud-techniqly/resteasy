﻿using System.Net.Http;
using NUnit.Framework;
using Techniqly.RestEasy.Tests.Mocks;

namespace Techniqly.RestEasy.Tests.UnitTests.Decorators
{
    [TestFixture]
    public class RequestDecoratorUnitTests
    {
        [Test]
        public void Execute_OkCallingWithNullRequest()
        {
            var decorator = new TestDecorator();
            decorator.Execute(null, new HttpRequestMessage());
        }

        [Test]
        public void Execute_OkCallingWithNullRequestMessage()
        {
            var decorator = new TestDecorator();
            decorator.Execute(new TestRequest(), null);
        }
    }
}