﻿using System;
using System.Net;
using System.Net.Http;
using Techniqly.RestEasy.Tests.Mocks;

namespace Techniqly.RestEasy.Tests.UnitTests.Decorators
{
    public class DecoratorUnitTests
    {
        protected TestRequest TestRequest { get; set; }
        protected static readonly string DefaultUri = "http://www.blah.com/api/widgets";

        protected MockDelegatingHandler CreateMockDelegatingHandler(Predicate<HttpRequestMessage> assertion)
        {
            return new MockDelegatingHandler(HttpStatusCode.OK) {Assertion = assertion};
        }
    }
}