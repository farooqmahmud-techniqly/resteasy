﻿using System.Net;
using System.Threading.Tasks;
using FluentAssertions;
using NUnit.Framework;
using Techniqly.RestEasy.Lib;

namespace Techniqly.RestEasy.Tests.UnitTests
{
    [TestFixture]
    public sealed class GetRequestUnitTests : RequestUnitTests
    {
        [SetUp]
        public void SetUp()
        {
            TestInitialize();
            Handler = CreateDefaultMockDelegatingHandler(_expectedContent, _expectedStatusCode);
            Request = new GetRequest(DefaultUri, Handler);
        }

        [TearDown]
        public void TearDown()
        {
            TestCleanup();
        }

        private static readonly string _expectedContent = "{\"models\":[\"435i\", \"640i\", \"i8\"]}";
        private static readonly HttpStatusCode _expectedStatusCode = HttpStatusCode.OK;


        [Test]
        public async Task GetContent()
        {
            var response = await Request.ExecuteAsync();
            
            response.Content.Should().Be(_expectedContent);
            response.StatusCode.Should().Be((int)_expectedStatusCode);
        }
    }
}