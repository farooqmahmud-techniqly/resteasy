﻿using System.Net;
using Techniqly.RestEasy.Lib;
using Techniqly.RestEasy.Tests.Mocks;

namespace Techniqly.RestEasy.Tests.UnitTests
{
    public abstract class RequestUnitTests
    {
        protected static readonly string DefaultUri = "http://www.foo.com/api/vehicles/bmw";
        protected Request Request { get; set; }
        protected MockDelegatingHandler Handler { get; set; }

        protected virtual void TestInitialize()
        {
        }

        protected virtual void TestCleanup()
        {
            Request.Dispose();
        }

        protected static MockDelegatingHandler CreateDefaultMockDelegatingHandler(HttpStatusCode expectedStatusCode)
        {
            return CreateDefaultMockDelegatingHandler(null, expectedStatusCode);
        }

        protected static MockDelegatingHandler CreateDefaultMockDelegatingHandler(string expectedContent, HttpStatusCode expectedStatusCode)
        {
            return new MockDelegatingHandler(expectedContent, expectedStatusCode);
        }
    }
}