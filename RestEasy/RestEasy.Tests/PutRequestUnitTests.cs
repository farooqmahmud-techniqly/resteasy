﻿using System.Net;
using System.Threading.Tasks;
using FluentAssertions;
using NUnit.Framework;
using Techniqly.RestEasy.Lib;
using Techniqly.RestEasy.Tests.UnitTests;

namespace Techniqly.RestEasy.Tests
{
    [TestFixture]
    public class PutRequestUnitTests : RequestUnitTests
    {
        [SetUp]
        public void SetUp()
        {
            TestInitialize();
            Handler = CreateDefaultMockDelegatingHandler(_content, _expectedStatusCode);
            Request = new PutRequest(DefaultUri, Handler) {Content = _content};
            Request.QueryParameters.Add("id", "1234");
        }

        [TearDown]
        public void TearDown()
        {
            TestCleanup();
        }

        private static readonly string _content = "{\"models\":[\"435i\", \"640i\", \"i8\"]}";
        private static readonly HttpStatusCode _expectedStatusCode = HttpStatusCode.OK;

        [Test]
        public async Task PutContent()
        {
            var response = await Request.ExecuteAsync();

            response.Content.Should().Be(_content);
            response.StatusCode.Should().Be((int) _expectedStatusCode);
        }
    }
}