﻿using System;
using FluentAssertions;
using NUnit.Framework;
using Techniqly.RestEasy.Lib.Credentials;

namespace Techniqly.RestEasy.Tests.Credentials
{
    [TestFixture]
    public class CredentialUnitTests
    {
        private static readonly string[] StringsNotSpecified = {string.Empty, " ", null};

        [Test]
        public void ConstructorThrowsWhenPasswordNotSpecified()
        {
            var exceptionsCaught = 0;

            foreach (var str in StringsNotSpecified)
            {
                try
                {
                    var cred = new Credential("user", str);
                }
                catch (ArgumentNullException)
                {
                    exceptionsCaught++;
                }
            }

            exceptionsCaught.Should().Be(StringsNotSpecified.Length);
        }

        [Test]
        public void ConstructorThrowsWhenUserIdNotSpecified()
        {
            var exceptionsCaught = 0;

            foreach (var str in StringsNotSpecified)
            {
                try
                {
                    var cred = new Credential(str, "123");
                }
                catch (ArgumentNullException)
                {
                    exceptionsCaught++;
                }
            }

            exceptionsCaught.Should().Be(StringsNotSpecified.Length);
        }
    }
}