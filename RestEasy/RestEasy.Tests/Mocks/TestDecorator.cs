﻿using System.Net.Http;
using Techniqly.RestEasy.Lib;
using Techniqly.RestEasy.Lib.Decorators;

namespace Techniqly.RestEasy.Tests.Mocks
{
    internal sealed class TestDecorator : RequestDecorator
    {
        protected override void DoExecute(Request request, HttpRequestMessage httpRequest)
        {
            
        }
    }
}