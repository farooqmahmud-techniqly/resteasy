﻿using System.Net;
using System.Net.Http;
using Techniqly.RestEasy.Lib;

namespace Techniqly.RestEasy.Tests.Mocks
{
    public class TestRequest : Request
    {
        public TestRequest() : this("http://www.foo.com", HttpMethod.Get, new MockDelegatingHandler(HttpStatusCode.OK))
        {
        }

        public TestRequest(string uri, HttpMethod httpMethod, DelegatingHandler httpHandler)
            : base(uri, httpMethod, httpHandler)
        {
        }
    }
}