﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using FluentAssertions;

namespace Techniqly.RestEasy.Tests.Mocks
{
    public sealed class MockDelegatingHandler : DelegatingHandler
    {
        private readonly HttpResponseMessage _response;

        public MockDelegatingHandler(HttpStatusCode expectedStatusCode) : this(null, expectedStatusCode)
        {
        }

        public MockDelegatingHandler(string expectedresponseContent, HttpStatusCode expectedStatusCode)
        {
            _response = new HttpResponseMessage(expectedStatusCode);

            if (!string.IsNullOrWhiteSpace(expectedresponseContent))
            {
                _response.Content = new StringContent(expectedresponseContent);
            }
        }

        internal Predicate<HttpRequestMessage> Assertion { get; set; }

        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request,
            CancellationToken cancellationToken)
        {
            Assertion?.Invoke(request).Should().Be(true);
            return Task.FromResult(_response);
        }
    }
}