﻿using System.Net;
using System.Threading.Tasks;
using FluentAssertions;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using Techniqly.RestEasy.Lib;

namespace Techniqly.RestEasy.Tests.Integration
{
    [TestFixture]
    public class RestEasyIntegrationTests
    {
        [Test]
        public async Task GetTheWeather()
        {
            var apiKey = "0bfd9c8ec382ac598e201a033426f6ef";
            var uri = "http://api.openweathermap.org/data/2.5/weather";

            var request = new GetRequest(uri);
            request.QueryParameters.Add("q", "Seattle");
            request.QueryParameters.Add("appid", apiKey);
            request.QueryParameters.Add("units", "imperial");

            var response = await request.ExecuteAsync();

            response.StatusCode.Should().Be((int) HttpStatusCode.OK);
            response.Content.Should().NotBeNullOrWhiteSpace();

            dynamic weather = JObject.Parse(response.Content);

            Assert.AreEqual(5809844, (int)weather.id);

        }

        [Test]
        public async Task SendPutToAttDeviceApi()
        {
            var uri = "http://api-m2x.att.com/v2/devices/1fb22939300352c067bcfb7ef8a35432/streams/speed/value";
            
            var request = new PutRequest(uri);
            request.CustomHeaders.Add("X-M2X-KEY", "51d56a1fe0dd8db5da8d1c57af72c64d");
            request.Content = @"{""value"":""100""}";

            var response = await request.ExecuteAsync();

            response.StatusCode.Should().Be((int) HttpStatusCode.Accepted);
        }

        [Test]
        public async Task SendPostToAttDeviceApi()
        {
            var uri = "http://api-m2x.att.com/v2/devices/1fb22939300352c067bcfb7ef8a35432/streams/speed/values";

            var request = new PostRequest(uri);
            request.CustomHeaders.Add("X-M2X-KEY", "51d56a1fe0dd8db5da8d1c57af72c64d");
            request.Content = @"{ ""values"": [ { ""timestamp"": ""2014-05-09T19:15:00.624Z"", ""value"": 32 }, { ""timestamp"": ""2014-05-09T20:15:00.522Z"", ""value"": 30 } ] }";

            var response = await request.ExecuteAsync();

            response.StatusCode.Should().Be((int)HttpStatusCode.Accepted);
        }
    }
}
